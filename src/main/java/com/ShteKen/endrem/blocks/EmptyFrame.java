package com.ShteKen.endrem.blocks;


import com.ShteKen.endrem.util.EyeTemplate;
import com.ShteKen.endrem.util.RegistryHandler;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.Item;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.common.ToolType;

import javax.annotation.Nonnull;
import java.util.Random;
import java.util.stream.Stream;

public class EmptyFrame extends Block {

    private static final DirectionProperty FACING = HorizontalBlock.HORIZONTAL_FACING;

    private static final VoxelShape SHAPE_N = Stream.of(makeCuboidShape(0, 0, 0, 16, 13, 16)).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();

    private static final VoxelShape SHAPE_E = Stream.of(makeCuboidShape(0, 0, 0, 16, 13, 16)).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();

    private static final VoxelShape SHAPE_S = Stream.of(makeCuboidShape(0, 0, 0, 16, 13, 16)).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();

    private static final VoxelShape SHAPE_W = Stream.of(makeCuboidShape(0, 0, 0, 16, 13, 16)).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();





    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        switch (state.get(FACING)) {
            case NORTH: default:
                return SHAPE_N;
            case EAST:
                return SHAPE_E;
            case SOUTH:
                return SHAPE_S;
            case WEST:
                return SHAPE_W;
        }

    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState().with(FACING, context.getPlacementHorizontalFacing().getOpposite());
    }

    @Override
    public BlockState rotate(BlockState state, Rotation rot) {
        return state.with(FACING, rot.rotate(state.get(FACING)));
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirrorIn) {
        return state.rotate(mirrorIn.toRotation(state.get(FACING)));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(FACING);
    }

    public EmptyFrame() {
        super(Block.Properties.create(Material.ROCK)
        .harvestTool(ToolType.PICKAXE)
        .harvestLevel(-1)
        .sound(SoundType.STONE)
                        .hardnessAndResistance(-1f, -1f)
        );
    }


    // Change the empty frame for one of the 11 frames
    @Nonnull
    @Override
    public ActionResultType  onBlockActivated (BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        for (int i = 0; i < RegistryHandler.FRAME_ARRAY.length ; i++){
            Item pearl = RegistryHandler.PEARL_ARRAY[i];
            Block frame = RegistryHandler.FRAME_ARRAY[i];

            if (player.getHeldItemMainhand().getItem() == pearl) {
                if (!worldIn.isRemote) {
                    if (!EyeTemplate.IsFrameOfType(worldIn, frame, pos)) {
                        Random r = new Random();
                        float randomResult = r.nextFloat();
                        worldIn.setBlockState(pos, frame.getDefaultState());
                        player.getHeldItemMainhand().getStack().shrink(1);
                        worldIn.playSound((PlayerEntity) null, player.getPosition(), SoundEvents.BLOCK_END_PORTAL_FRAME_FILL, SoundCategory.BLOCKS, 0.3F, randomResult);

                    }
                }
            }
        }
        return ActionResultType.PASS;
    }

}


