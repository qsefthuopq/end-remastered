package com.ShteKen.endrem.config;

import com.ShteKen.endrem.EndRemastered;
import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import com.electronwill.nightconfig.core.io.WritingMode;
import net.minecraftforge.common.ForgeConfigSpec;

import java.nio.file.Path;

public class Config {
    private static final ForgeConfigSpec.Builder CONFIG = new ForgeConfigSpec.Builder();
    private static ForgeConfigSpec COMMON_CONFIG;

    public static ForgeConfigSpec.ConfigValue<Boolean> ENABLE_ENDER_EYES;
    public static ForgeConfigSpec.ConfigValue<Boolean> END_CASTLE_ENABLED;
    public static ForgeConfigSpec.ConfigValue<Boolean> END_GATE_ENABLED;
    public static ForgeConfigSpec.ConfigValue<Integer> END_CASTLE_DISTANCE;
    public static ForgeConfigSpec.ConfigValue<Integer> END_GATE_DISTANCE;
    public static ForgeConfigSpec.ConfigValue<Integer> END_GATE_SIZE;
    public static ForgeConfigSpec.ConfigValue<Integer> EYES_LOCATE_STRUCTURE;
    public static ForgeConfigSpec.ConfigValue<Integer> MAP_LOCATES_STRUCTURE;

    static {
        initConfig();
    }
    
    private static void initConfig() {
        CONFIG.push(EndRemastered.MOD_ID);
        ENABLE_ENDER_EYES = CONFIG.comment("Enable/Disable usage of Ender Eyes").define("enable_ender_eyes", false);
        END_CASTLE_ENABLED = CONFIG.comment("Enable/Disable the End Castle").define("enable_end_castle", true);
        END_GATE_ENABLED = CONFIG.comment("Enable/Disable the End Gate").define("enable_end_gate", true);
        END_CASTLE_DISTANCE = CONFIG.comment("Distance in chunks between End Castles").define("castle_distance", 380);
        END_GATE_DISTANCE = CONFIG.comment("Distance in chunks between End Gates").define("end_gate_distance", 380);
        END_GATE_SIZE = CONFIG.comment("Size of the End Gates").define("end_gate_size", 20);
        EYES_LOCATE_STRUCTURE = CONFIG.comment("Sets the Behavior of the Custom Eyes (0 for disabled, 1 to locate Castle and 2 to locate End Gate)").define("eye_behavior", 2);
        MAP_LOCATES_STRUCTURE = CONFIG.comment("Sets the Behavior of the Custom Map (0 for disabled, 1 to locate Castle and 2 to locate End Gate)").define("map_behavior", 1);


        CONFIG.pop();
        COMMON_CONFIG = CONFIG.build();

    }

    public static void setup(Path path) {
        final CommentedFileConfig configData = CommentedFileConfig.builder(path)
                .sync()
                .autosave()
                .writingMode(WritingMode.REPLACE)
                .build();

        configData.load();
        COMMON_CONFIG.setConfig(configData);
    }
}
