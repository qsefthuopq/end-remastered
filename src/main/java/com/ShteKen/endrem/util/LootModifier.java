package com.ShteKen.endrem.util;

import net.minecraft.loot.LootPool;
import net.minecraft.loot.TableLootEntry;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.LootTableLoadEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber
public class LootModifier {

    @SubscribeEvent
    public void addLoot(LootTableLoadEvent event) {
        String lootTable = event.getName().toString();
        System.out.println("endrem loot_table_load: " + lootTable);

        switch (lootTable) {
            case "minecraft:chests/desert_pyramid":
            case "minecraft:chests/buried_treasure":
            case "minecraft:chests/abandoned_mineshaft":
            case "minecraft:chests/igloo_chest":
            case "minecraft:chests/jungle_temple":
            case "minecraft:chests/nether_bridge":
            case "minecraft:chests/pillager_outpost":
                event.getTable().addPool(createPool("endrem:minecraft_chest/" + lootTable.split("/")[1], "endrem_loot_" + lootTable.split("/")[1]));
                break;

            case "minecraft:entities/elder_guardian":
            case "minecraft:entities/evoker":
            case "minecraft:entities/wither":
                event.getTable().addPool(createPool("endrem:minecraft_entities/" + lootTable.split("/")[1], "endrem_loot_" + lootTable.split("/")[1]));
                break;
        }
    }

    private static LootPool createPool(String location, String name) {
        return LootPool.builder().name(name)
                .addEntry(TableLootEntry.builder(new ResourceLocation(location)).weight(1)).build();
    }
}
