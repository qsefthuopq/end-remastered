package com.ShteKen.endrem.items;

import com.ShteKen.endrem.EndRemastered;
import com.ShteKen.endrem.config.Config;
import com.ShteKen.endrem.world.STConfig.STJStructures;
import com.ShteKen.endrem.util.RegistryHandler;
import com.ShteKen.endrem.world.STConfig.STStructures;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.block.Block;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.projectile.EyeOfEnderEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Rarity;
import net.minecraft.stats.Stats;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.loading.FMLPaths;

import javax.annotation.Nullable;
import java.util.List;

public class CustomPearl extends Item {
    public static Structure<NoFeatureConfig> structureToLocate;

    public CustomPearl() {
        super(new Item.Properties().isImmuneToFire().maxStackSize(16).rarity(Rarity.EPIC).group(EndRemastered.TAB));
    }

    public static Structure<NoFeatureConfig> getStructureToLocate() {
        Config.setup(FMLPaths.CONFIGDIR.get().resolve(EndRemastered.MOD_ID + ".toml"));
        switch (Config.EYES_LOCATE_STRUCTURE.get()) {
            case 0:
                structureToLocate = null;
                break;
            case 1:
                structureToLocate = STStructures.END_CASTLE.get();
                break;
            case 2: default:
                structureToLocate = STJStructures.END_GATE.get();
                break;
        }
        return structureToLocate;
    }

    @OnlyIn(Dist.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        String translationKey = String.format("item.endrem.%s.description", this.getItem());
        tooltip.add(new TranslationTextComponent(translationKey));
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        ItemStack itemstack = playerIn.getHeldItem(handIn);
        BlockRayTraceResult raytraceresult = rayTrace(worldIn, playerIn, RayTraceContext.FluidMode.NONE);
        boolean lookingAtFrame = false;


        for (Block frame : RegistryHandler.FRAME_ARRAY) {
            if (raytraceresult.getType() == RayTraceResult.Type.BLOCK && worldIn.getBlockState(raytraceresult.getPos()).isIn(frame)) {
                lookingAtFrame = true;
            }
        }

        if (lookingAtFrame) {
            return ActionResult.resultPass(itemstack);
        } else {
            playerIn.setActiveHand(handIn);
            if (worldIn instanceof ServerWorld) {
                BlockPos blockpos = ((ServerWorld) worldIn).getChunkProvider().getChunkGenerator().func_235956_a_((ServerWorld) worldIn, getStructureToLocate(), playerIn.getPosition(), 100, false);
                if (blockpos != null) {
                    EyeOfEnderEntity eyeofenderentity = new EyeOfEnderEntity(worldIn, playerIn.getPosX(), playerIn.getPosYHeight(0.5D), playerIn.getPosZ());
                    eyeofenderentity.func_213863_b(itemstack);
                    eyeofenderentity.moveTowards(blockpos);
                    eyeofenderentity.shatterOrDrop = true;

                    worldIn.addEntity(eyeofenderentity);
                    if (playerIn instanceof ServerPlayerEntity) {
                        CriteriaTriggers.USED_ENDER_EYE.trigger((ServerPlayerEntity) playerIn, blockpos);
                    }

                    worldIn.playSound((PlayerEntity) null, playerIn.getPosition(), SoundEvents.ENTITY_ENDER_EYE_LAUNCH, SoundCategory.NEUTRAL, 0.5F, 0.4F / (random.nextFloat() * 0.4F + 0.8F));
                    worldIn.playEvent((PlayerEntity) null, 1003, playerIn.getPosition(), 0);

                    if (!playerIn.abilities.isCreativeMode) {
                        itemstack.shrink(1);
                    }

                    playerIn.addStat(Stats.ITEM_USED.get(this));
                    playerIn.swing(handIn, true);
                    return ActionResult.resultSuccess(itemstack);
                }
            }
            return ActionResult.resultConsume(itemstack);
        }
    }
    @Override
    public boolean isDamageable(ItemStack stack)
    {
        return !this.getItem().isDamageable();
    }

}
